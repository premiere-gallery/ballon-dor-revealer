const { TezosToolkit, OpKind } = require('@taquito/taquito');
const { InMemorySigner } = require('@taquito/signer');
const metadata = require('./token_metadata.json');
const { char2Bytes } = require('@taquito/utils');
const ipfsClient = require('ipfs-http-client');
const cliProgress = require("cli-progress");

const dotenv = require('dotenv');
dotenv.config();

const PYRITES = process.env.PYRITES;
const NODE = process.env.TEZOS_NODE;
const PRIVATE_KEY = process.env.REVEALER_PRIVATE_KEY;

const batch = process.env.BATCH_SIZE;

async function pyrite_reveal(){
    // START
    console.info("----START----")
    const from = 0, to = (process.env.LIMIT || 100) - 1

    let batch_size = batch;
    if(to - from < batch_size) batch_size = to - from;

    const first = parseInt(metadata[0].name.replace('Pyrite #',''));
    const last = parseInt(metadata[metadata.length - 1].name.replace('Pyrite #',''));
    console.log(`${first} vs. ${from}`)
    console.log(`${last} vs. ${to}`)
    if(from < first || to > last) throw 'Metadata unavailable for given boundaries';
        
    const Tezos = new TezosToolkit(NODE);
    const signer = await InMemorySigner.fromSecretKey(PRIVATE_KEY);
    Tezos.setProvider({ signer: signer });
    const contract = await Tezos.contract.at(PYRITES);

    const totalBatches = (to - from) / batch_size;
    console.log(`~ Publishing Pyrites Metadata from #${from} to #${to} in ${totalBatches} batches of ${batch_size}`);

    let start = from;
    let end = from + batch_size;
    const auth = 'Basic ' + Buffer.from(process.env.PROJECT_ID + ':' + process.env.PROJECT_SECRET).toString('base64');

    let numBatch = 1;

    while(start < end){
        tx_batch = []

        console.log(`\n BUILDING BATCH ${numBatch}: #${start} to #${end}`);
        
        const barBatch = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
        barBatch.start(end - start, 0);

        j = 0;
               
        for(let i = start; i <= end; i++){
            try{
                temp_metadata = JSON.stringify(metadata[i - first])
            }
            catch(error){
                console.error(`Error: metadata for pyrit#${i} has not been generated`);
            }
            const client = await ipfsClient.create({
                host: process.env.INFURA_GATEWAY,
                port: process.env.INFURA_PORT,
                protocol: 'https',
                headers: {
                    authorization: auth
                }
            })
            const ipfsToken = await client.add(temp_metadata)
            tx_batch.push({
            kind: OpKind.TRANSACTION,
            ...contract.methods.update_token_metadata(i, char2Bytes(`ipfs://${ipfsToken.path}`)).toTransferParams() 
            });
            barBatch.update(j++);           
        }

        barBatch.stop();

        try{
            console.log(' sending batch...');
            const batch = Tezos.wallet.batch(tx_batch); 
            const batchOp = await batch.send();
            await batchOp.confirmation();
            console.log(' Operation hash:', batchOp.opHash);
        }
        catch(error){
            console.error('Batch transaction failed');
            console.error(error);
            break;
        }

        start += batch_size;
        end += batch_size;
        if(end > to) end = to;

        numBatch++;
    }
}

try {
    pyrite_reveal();
}
catch (error) {
    console.error(error);
}