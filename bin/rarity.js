const metadata = require('./token_metadata.json')
const nb_items = metadata.length

let main_composite = {}, second_composite = {}, background = {}, shape = {}, origin = {}, localization = {}, hardness = {}

for(let entry of metadata){
    if(main_composite[entry.attributes[0].name])
        main_composite[entry.attributes[0].name] += 1
    else
        main_composite[entry.attributes[0].name] = 1
    
    if(second_composite[entry.attributes[1].name])
        second_composite[entry.attributes[1].name] += 1
    else if(entry.attributes[1].name != "Background")
        second_composite[entry.attributes[1].name] = 1

    let index = entry.attributes[1].name != "Background"? 2 : 1
    
    if(background[entry.attributes[index].value])
        background[entry.attributes[index].value] += 1
    else
        background[entry.attributes[index].value] = 1

    if(shape[entry.attributes[index + 1].value])
        shape[entry.attributes[index + 1].value] += 1
    else
        shape[entry.attributes[index + 1].value] = 1

    if(origin[entry.attributes[index + 2].value])
        origin[entry.attributes[index + 2].value] += 1
    else
        origin[entry.attributes[index + 2].value] = 1

    if(localization[entry.attributes[index + 3].value])
        localization[entry.attributes[index + 3].value] += 1
    else
        localization[entry.attributes[index + 3].value] = 1

    if(hardness[entry.attributes[index + 4].value])
        hardness[entry.attributes[index + 4].value] += 1
    else
        hardness[entry.attributes[index + 4].value] = 1
}

for(let key of Object.keys(main_composite)){
    main_composite[key] = `${parseFloat(main_composite[key] / nb_items * 100).toFixed(4)}%`
}
console.log(main_composite)

for(let key of Object.keys(second_composite)){
    second_composite[key] = `${parseFloat(second_composite[key] / nb_items * 100).toFixed(4)}%`
}
console.log(second_composite)

for(let key of Object.keys(background)){
    background[key] = `${parseFloat(background[key] / nb_items * 100).toFixed(4)}%`
}
console.log(background)

for(let key of Object.keys(shape)){
    shape[key] = `${parseFloat(shape[key] / nb_items * 100).toFixed(4)}%`
}
console.log(shape)

for(let key of Object.keys(origin)){
    origin[key] = `${parseFloat(origin[key] / nb_items * 100).toFixed(4)}%`
}
console.log(origin)

for(let key of Object.keys(localization)){
    localization[key] = `${parseFloat(localization[key] / nb_items * 100).toFixed(4)}%`
}
console.log(localization)

for(let key of Object.keys(hardness)){
    hardness[key] = `${parseFloat(hardness[key] / nb_items * 100).toFixed(4)}%`
}
console.log(hardness)