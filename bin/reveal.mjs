import * as dotenv from 'dotenv'
import { TezosToolkit } from "@taquito/taquito"
import { SingleBar, Presets} from "cli-progress"
import * as fs from 'fs'
const rewards = JSON.parse(fs.readFileSync("./bin/rewards.json"));
const visuals = JSON.parse(fs.readFileSync("./bin/visuals.json"));
const token_metadata = JSON.parse(fs.readFileSync("./bin/token_metadata.json"));
dotenv.config()

const PYRITES = process.env.PYRITES || "KT1E9vnFywiWig3xpp9NDj1k7U6UFMFGqhrT"
const TEZOS_NODE = process.env.TEZOS_NODE || "https://sceme.otori.io"
const ARTIST = process.env.ARTIST || "tz1Xur15cBJSsKU3iDfnWg7sQmk5cjkhashP"
const LIMIT = process.env.LIMIT || 100
const composites = [
        {name: "Copper", occurence: 25},
        {name: "Silver", occurence: 15},
        {name: "Gold", occurence: 10},
        {name: "Iron", occurence: 25},
        {name: "Arsenic", occurence: 10},
        {name: "Zinc", occurence: 10},
        {name: "Nickel", occurence: 5}
    ]
const origins = [
        {name: "Sedimentation", occurence: 50},
        {name: "Hydrothermal", occurence: 30},
        {name: "Metamorphism", occurence: 15},
        {name: "Magma", occurence: 4},
        {name: "Meteorite", occurence: 1}
    ]
const localization = [
        {name: "Europe", occurence: 50},
        {name: "Africa", occurence: 20},
        {name: "Asia", occurence: 10},
        {name: "Central America", occurence: 10},
        {name: "South America", occurence: 6},
        {name: "North America", occurence: 3},
        {name: "Oceania", occurence: 1},
    ]
const hardness = [
        {name: "6", occurence: 40},
        {name: "6.1", occurence: 30},
        {name: "6.2", occurence: 15},
        {name: "6.3", occurence: 10},
        {name: "6.4", occurence: 4},
        {name: "6.5", occurence: 1},
    ]

// START
console.info("----START----")
const Tezos = new TezosToolkit(TEZOS_NODE)  
const contract = await Tezos.contract.at(PYRITES)
const storage = await contract.storage()
const default_token_metadata = storage.default_token_metadata
console.info(`Default token metadata: ${default_token_metadata}`)
const all_tokens = storage.all_tokens
const limit_supply = storage.limit_supply
console.info(`NB Tokens: ${all_tokens}`)
let i = 0, unrevealed = [], selected_visuals = [], selected_rewards = []
const bar1 = new SingleBar({}, Presets.shades_classic);
bar1.start(LIMIT, 0);
while(i < LIMIT){
    unrevealed.push(i)
    i++
    bar1.update(i);
}
bar1.stop();

let nb_revealed = all_tokens - unrevealed.length
const nb_unprocessed = limit_supply - nb_revealed
console.info(`NB Limit supply: ${limit_supply}`)
console.info(`NB Unrevealed: ${unrevealed.length}`)
console.info(`NB Revealed: ${nb_revealed}`)
console.info(`NB unprocessed: ${nb_unprocessed}`)
console.info(`Selecting random rewards`)

let visuals_set = []
for(let key of Object.keys(visuals)){
    visuals_set.push.apply(visuals_set, [...Array(visuals[key].quantity).fill(parseInt(key))])
}

let rewards_set = []
for(let key of Object.keys(rewards)){
    rewards_set.push.apply(rewards_set, [...Array(rewards[key].quantity).fill(parseInt(key))])
}

let composites_set = []
for(let entry of composites){
    composites_set.push.apply(composites_set, [...Array(entry.occurence).fill(entry.name)])
}

let origins_set = []
for(let entry of origins){
    origins_set.push.apply(origins_set, [...Array(entry.occurence).fill(entry.name)])
}

let localization_set = []
for(let entry of localization){
    localization_set.push.apply(localization_set, [...Array(entry.occurence).fill(entry.name)])
}

let hardness_set = []
for(let entry of hardness){
    hardness_set.push.apply(hardness_set, [...Array(entry.occurence).fill(entry.name)])
}

i = 0
while(i < unrevealed.length){
    let rd = Math.floor(Math.random() * (rewards_set.length - 1))
    selected_rewards.push(rewards_set[rd])
    if(rewards_set[rd] === 0)
        selected_visuals.push(99)
    else{
        let rd2 = Math.floor(Math.random() * (visuals_set.length - 1))
        selected_visuals.push(visuals_set[rd2])
        visuals_set.splice(rd2, 1)
    }
    rewards_set.splice(rd, 1)
    i++
}

i = 0
let refined_rewards = rewards, refined_visuals = visuals, tk_metadatas = token_metadata
console.log(`#${selected_rewards.length} to process`)
while(i < selected_rewards.length){
    if(refined_visuals[selected_visuals[i]]){
        let formats = [{
            mimeType: `video/mp4`,
            uri: visuals[selected_visuals[i]].ipfs
        },{
            mimeType: `image/jpg`,
            uri: visuals[selected_visuals[i]].thumbnail
        }]
        // Build composition
        let attributes = []
        let composite = composites_set[Math.floor(Math.random() * 99)]
        if(composite === visuals[selected_visuals[i]].Metal){
            attributes.push({name: composite, value: `100%`})
        }else{
            let cmp = Math.floor(Math.random() * 48) + 1
            attributes.push({name: visuals[selected_visuals[i]].Metal, value: `${100 - cmp}%`})
            attributes.push({name: composite, value: `${cmp}%`})
        }
        attributes.push({name: "Background", value: visuals[selected_visuals[i]].Background})
        attributes.push({name: "Shape", value: visuals[selected_visuals[i]].Shape})
        attributes.push({name: "Origin", value: origins_set[Math.floor(Math.random() * 99)]})
        attributes.push({name: "Localization", value: localization_set[Math.floor(Math.random() * 99)]})
        attributes.push({name: "Hardness", value: hardness_set[Math.floor(Math.random() * 99)]})
        if(rewards[selected_rewards[i]].en !== "-")
            attributes.push({name: "Reward", value: rewards[selected_rewards[i]].en})

        const token_data = {
            name: `Pyrite #${i + nb_revealed}`,
            decimals: 0,
            description: rewards[selected_rewards[i]].en === "-" ? 
                `Ballon d'Or® NFT from the 2022 Pyrite Fragments collection designed by Leo Caillard. \nAll utilities and information on nft.ballondor.com. \nBe careful utilities may have been already claimed. \nJoin the community on Ballon d’Or® Discord. \nGet official token information & status from https://nft.ballondor.com/pyrite/${i + nb_revealed}` 
            : 
                `Ballon d'Or® NFT from the 2022 Pyrite Fragments collection designed by Leo Caillard. \nAll utilities and information on nft.ballondor.com. \nBe careful utilities may have been already claimed. \nJoin the community on Ballon d’Or® Discord. \nExclusive reward : ${rewards[selected_rewards[i]].en}. \nGet official token information, reward claim & status from https://nft.ballondor.com/pyrite/${i + nb_revealed}`,
            thumbnailUri: visuals[selected_visuals[i]].thumbnail,
            displayUri: visuals[selected_visuals[i]].thumbnail,
            artifactUri: visuals[selected_visuals[i]].ipfs,
            image: visuals[selected_visuals[i]].thumbnail,
            creators: [ARTIST],
            amount: 1,
            extension: "mp4",
            attributes,
            date: Date.now(),
            symbol: "PYRITE",
            isBooleanAmount: true,
            shouldPreferSymbol: false,
            royalties: {decimals: 4, shares: {"tz1WyZ4mwysxkgiMbRQn5DXzwxw9c6eVdc42": "500", "tz1Xur15cBJSsKU3iDfnWg7sQmk5cjkhashP": "200", "tz1hCKMo9r9sb2epHzUBZgQKCX1uNAY32vM7": "50" }},
            rights: "(c) Editions P. Amaury. All Rights Reserved.",
            formats
        }
        refined_rewards[selected_rewards[i]].quantity -= 1
        refined_visuals[selected_visuals[i]].quantity -= 1
        tk_metadatas.push(token_data)
    }else{
        console.log("------------------------------")
        console.log(`Winning ticket ${i + nb_revealed}`)
        console.log(rewards[selected_rewards[i]].en)
        console.log(selected_visuals[i])
        console.log("------------------------------")
        let formats = [{
            mimeType: `video/mp4`,
            uri: "ipfs://QmSmakoBAZomrncDXArkqcHq2dk9CsQhfbpR1zFtSRaBb4"
        },{
            mimeType: `image/jpg`,
            uri: "ipfs://QmZNf89ZZkq7XwrQub6fTtrHEQGbqCuew78KkASctjU3E1"
        }], attributes = []
        attributes.push({name: "Diamond", value: "100%"})
        attributes.push({name: "Background", value: "Gold marble"})
        attributes.push({name: "Shape", value: "2"})
        attributes.push({name: "Origin", value: "Meteorite"})
        attributes.push({name: "Localization", value: "Oceania"})
        attributes.push({name: "Hardness", value: "6.6"})
        attributes.push({name: "Reward", value: rewards[selected_rewards[i]].en})
        const token_data = {
            name: `Pyrite #${i + nb_revealed}`,
            decimals: 0,
            description: `Ballon d'Or® NFT from the 2022 Pyrite Fragments collection designed by Leo Caillard. \nAll utilities and information on nft.ballondor.com. \nBe careful utilities may have been already claimed. \nJoin the community on Ballon d’Or® Discord. \nExclusive reward : ${rewards[selected_rewards[i]].en}. \nGet official token information, reward claim & status from https://nft.ballondor.com/pyrite/${i + nb_revealed}`,
            thumbnailUri: "ipfs://QmZNf89ZZkq7XwrQub6fTtrHEQGbqCuew78KkASctjU3E1",
            displayUri: "ipfs://QmZNf89ZZkq7XwrQub6fTtrHEQGbqCuew78KkASctjU3E1",
            artifactUri: "ipfs://QmSmakoBAZomrncDXArkqcHq2dk9CsQhfbpR1zFtSRaBb4",
            image: "ipfs://QmZNf89ZZkq7XwrQub6fTtrHEQGbqCuew78KkASctjU3E1",
            creators: [ARTIST],
            amount: 1,
            extension: "mp4",
            attributes,
            date: Date.now(),
            symbol: "PYRITE",
            isBooleanAmount: true,
            shouldPreferSymbol: false,
            royalties: {decimals: 4, shares: {"tz1WyZ4mwysxkgiMbRQn5DXzwxw9c6eVdc42": "500", "tz1Xur15cBJSsKU3iDfnWg7sQmk5cjkhashP": "200", "tz1hCKMo9r9sb2epHzUBZgQKCX1uNAY32vM7": "50" }},
            rights: "(c) Editions P. Amaury. All Rights Reserved.",
            formats
        }
        tk_metadatas.push(token_data)
        refined_rewards[selected_rewards[i]].quantity -= 1
    }
    i++
}
fs.writeFileSync('./bin/token_metadata.json', JSON.stringify(tk_metadatas))
fs.writeFileSync('./bin/rewards.json', JSON.stringify(refined_rewards))
fs.writeFileSync('./bin/visuals.json', JSON.stringify(refined_visuals))
console.info("----END----")