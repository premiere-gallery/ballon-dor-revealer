# Pyrite Fragments Ballon d'Or® 2022 Reveal

This open sourced project is used to reveal visuals and rewards attached to NFT Fragments. [NodeJS](https://nodejs.org/en/) and [npm](https://docs.npmjs.com/cli/v8/commands/npm-install) are required to run **reveal & publish**

## Rationale
Based on `all_tokens` in the circulating supply, and a `LIMIT`, the **reveal script** will fetch random visuals from `visuals.json` and a random reward from `rewards.json`.

## Environment
Create a `.env` file at project root. Edit as `.env.example` with your own variables e.g., `PYRITES contract hash`, `TEZOS NODE`, `LIMIT`, etc... Edit `rewards.json` last entry to match the `LIMIT`.

## Init
Run `npm i` to install dependencies

## Reveal
Run locally with `npm run reveal`

It will randomly build `token_metadata.json` that will be processed by the **publish script**

## Publish
Run locally with `npm run publish`* **

*The **publish script** can only be operated from the `Administrator` wallet or `Revealer`wallet. Click [here](https://gitlab.com/premiere-gallery/ballondor-contracts) for more info regarding the smart contract.

**It is required to have an active [Infura](https://infura.io/) account and API IPFS keys

### Note
This project was designed and published during the development and launch of [Ballon d'Or® NFT](https://nft.ballondor.com).
